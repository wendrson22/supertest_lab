const chai = require('chai');

const rotaProdutos = 'produtos'

describe('Validar verbo GET no endpoint ' + rotaProdutos, () => {
  it('Deve buscar os produtos com sucesso', async () => {

    const { body } = await request.get(rotaProdutos).expect(200)
    chai.assert.isNotEmpty(body)
  })

  it('Validar retorno com sucesso ao utilizar query string', async () => {

    const { body } = await request.get(rotaProdutos)
    const idPrimeiroProduto = body.produtos[0]._id

    const resProdutoById = await request.get(rotaProdutos).query({ _id: idPrimeiroProduto }).expect(200)
    chai.assert.isNotEmpty(resProdutoById.body)
  })
})