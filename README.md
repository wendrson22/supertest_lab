# supertest-lab

An API test automation setup that you can apply in the real world.

## Features

### Test suite

- [x] [ServeRest](https://github.com/ServeRest/ServeRest/), an amazing server that simulates an e-commerce to be tested
- [x] [Mocha](https://mochajs.org/), to manage tests
- [x] [Supertest](https://www.npmjs.com/package/supertest), to create and run API tests
- [x] [Chai](https://www.chaijs.com/), to make assertions
- [x] [Faker](https://www.npmjs.com/package/faker), to generate randon data for tests
- [ ] "Page Action" custom pattern, to organize the test structure
- [x] Environment config, to run tests in multiple environment

### Code quality

- [ ] [Prettier](https://www.npmjs.com/package/prettier) - to format the code
- [ ] [ESLint](https://www.npmjs.com/package/eslint), to identify wrong patterns in the code
- [ ] [Husky](https://www.npmjs.com/package/husky), to check and improve commits
- [ ] [commitlint](https://commitlint.js.org/#/), to check and improve commit messages

### CI/CD

- [ ] [GitLab CI/DC](https://docs.gitlab.com/ee/ci/), to automatically trigger test execution in CI/CD
- [ ] [mochawesome](https://www.npmjs.com/package/mochawesome), to provide feedback about test execution

## Setup

1. Clone and access the cloned repo folder

   `$ git clone git@git.tray.net.br:wenderson.silva/supertest_lab.git && cd supertest-lab`

2. Install the project dependencies

   `$ npm install`

3. Execution Examples

   `$ npm run test`
